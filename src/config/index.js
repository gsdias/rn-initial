import * as firebase from 'firebase'
import { GoogleAnalyticsTracker } from 'react-native-google-analytics-bridge'
import FirebaseCrash from 'react-native-firebase-crash-report'
import packageFile from '../../package.json'

export const tracker = new GoogleAnalyticsTracker('*****')

const firebaseConfig = {
  apiKey: '*****',
  authDomain: '*****.firebaseapp.com',
  databaseURL: 'https://*****.firebaseio.com',
  projectId: '*****',
  storageBucket: '*****.appspot.com',
  messagingSenderId: '*****',
}

export const version = packageFile.version

export const firebaseApp = firebase.initializeApp(firebaseConfig)
export const firebaseAuth = firebaseApp.auth()
export const firebaseDb = firebaseApp.database()
