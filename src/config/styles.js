module.exports = {
  header: {
    icon: {
      color: '#333',
    },
  },
  text: {
    empty: {
      fontFamily: 'OpenSans-Semibold',
      fontSize: 15,
      color: '#333',
    },
    notEmpty: {
      fontFamily: 'OpenSans',
      fontSize: 21,
      color: '#333',
    },
  },
}
