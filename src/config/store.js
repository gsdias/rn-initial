import { AsyncStorage } from 'react-native'
import devTools from 'remote-redux-devtools'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { persistStore, autoRehydrate } from 'redux-persist'
import reducer from '../reducers'
import promise from '../lib/promise'

export default function configureStore(onCompletion:()=>void):any {
  const enhancer = compose(
    applyMiddleware(thunk, promise),
    autoRehydrate({ log: true }),
    devTools({
      name: 'CurrencyRates', realtime: true,
    }),
  )

  const store = createStore(reducer, enhancer)
  const blacklist = ['tabBar']
  persistStore(store, { storage: AsyncStorage, blacklist }, onCompletion)

  return store
}
