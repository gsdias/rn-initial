export const CONT_ACTION = 'CONT_ACTION'

export function action(payload) {
  return {
    type: CONT_ACTION,
    payload,
  }
}
