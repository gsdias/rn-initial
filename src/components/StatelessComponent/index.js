import React from 'react'
import { Text } from 'react-native'
import commonStyles from '../../config/styles'

export default (props) => {
  const style = props.style || {}
  return (<Text
    style={{
      ...commonStyles.header.Icon,
      ...style,
    }}
  >{props.children}</Text>)
}
