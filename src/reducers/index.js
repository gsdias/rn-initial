import { combineReducers } from 'redux'
import { tabBarReducer } from '../routes/navigationConfiguration'
import section from './section'

export default combineReducers({
  tabBar: tabBarReducer,
  section,
})
