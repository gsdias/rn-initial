import {
  CONT_ACTION,
} from '../actions/section'

export type State = {
    value: string,
}

const initialState = {
  value: 'Default Value',
}

export default function (state:State = initialState, action) {
  if (action.type === CONT_ACTION) {
    return { ...state,
      value: action.payload,
    }
  }

  return state
}
