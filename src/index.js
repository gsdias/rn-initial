import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { Provider } from 'react-redux'
import store from './config/store'
import TabBarNavigation from './routes'

class Root extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isLoading: true,
      store: store(() => {
        this.setState({ isLoading: false })
      }),
    }
  }

  componentWillUnmount() {
    
  }

  render() {
    if (this.state.isLoading) {
      return <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
        <ActivityIndicator animating color={'#000'} size={'large'} />
      </View>
    }
    return (
      <Provider store={this.state.store}>
        <TabBarNavigation />
      </Provider>
    )
  }
}

export default Root
