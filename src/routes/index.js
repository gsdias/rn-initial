import React from 'react'
import { addNavigationHelpers } from 'react-navigation'
import { connect } from 'react-redux'
import { TabBar } from './navigationConfiguration'

const mapStateToProps = state => ({
  navigationState: state.tabBar,
})

const TabBarNavigation = (props) => {
  const { dispatch, navigationState } = props

  return (
    <TabBar
      navigation={
        addNavigationHelpers({
          dispatch,
          state: navigationState,
        })
      }
    />
  )
}

export default connect(mapStateToProps)(TabBarNavigation)
