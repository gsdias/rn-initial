import { StackNavigator } from 'react-navigation'

import { tracker } from '../config'

/* import of layouts */
import Layout from '../layouts/Layout'

const screenName = navState => navState ? navState.routes[navState.index].routeName : void 0

export const TabBar = StackNavigator({
  Layout: {
    screen: Layout,
    path: 'layout/:param',
  },
}, {
  initialRouteName: 'Layout',
  headerMode: 'none',
})

export const tabBarReducer = (state, action) => {
  if (action.type === 'JUMP_TO_TAB') {
    return {
      ...state,
      index: 0,
    }
  }
  return TabBar.router.getStateForAction(action, state)
}

TabBar.prototype.componentDidUpdate = function (prevProps) {
  const currScreen = screenName(this.props.navigation.state)
  const prevScreen = screenName(prevProps.navigation.state)
  if (!!currScreen && currScreen !== prevScreen) {
    tracker.trackScreenView(currScreen)
  }
}
