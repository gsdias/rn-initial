import { connect } from 'react-redux'
import { Platform, NativeModules } from 'react-native'
import View from './Layout'
import i18n from '../../config/i18n'
import { action } from '../../actions/section'
import { func } from '../../lib'

const props = s => {
  const language = Platform.OS === 'ios' ?
    NativeModules.SettingsManager.settings.AppleLocale.split('_') :
    NativeModules.I18nManager.localeIdentifier.split('_')

  return {
    section: s.section,
    i18n: i18n[language],
    func,
  }
}

function actions(dispatch) {
  return {
    action: (p) => dispatch(action(p)),
  }
}

export default connect(props, actions)(View)
