import React, { Component } from 'react'
import { Image, Button, View } from 'react-native'
import { StatelessComponent } from '../../components'
import images from '../../config/images'
import style from './style'

export default class extends Component {
  static defaultProps = {
    name: '',
  }

  constructor(props) {
    super(props)

    this.state = {
      valueChanged: 'state default'
    }

    this.onPress = this.onPress.bind(this)
  }

  onPress() {
    this.setState({ valueChanged: 'state changed' })
    this.props.func({})
    this.props.action('Changed value')
  }

  render() {
    const { i18n, navigation } = this.props
    const { navigate } = navigation
    
    return (
      <View style={style.container} testID="Screen">
        <Button onPress={this.onPress} title="test" />
        <StatelessComponent>{this.state.valueChanged} - {this.props.section.value}</StatelessComponent>
        <Image source={images.demo} resizeMode="contain" style={{ flex: 1 }} />
      </View>
    )
  }
}
